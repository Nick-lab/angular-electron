const { app, BrowserWindow, ipcMain } = require('electron')

let win;

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 450,
    minWidth: 300,
    minHeight: 200,
    backgroundColor: '#ffffff',
    frame: false
  })
  win.loadURL(`file://${__dirname}/dist/angular-electron/index.html`)

  //// uncomment below to open the DevTools.

  win.webContents.openDevTools()

  // Event when the window is closed.
  win.on('closed', function () {
    win = null
  })
}

ipcMain.on('set-name', (e, name) => {
  e.sender.send('window-rename', name);
});
// Create window on electron intialization
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // macOS specific close process
  if (win === null) {
    createWindow()
  }
})
