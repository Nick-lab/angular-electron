import { Component } from '@angular/core';

declare var electron: any;
const { BrowserWindow } = electron.remote;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  ipc: any = electron.ipcRenderer;

  title = 'app';

  constructor() {
    document.title = this.title;
  }

  onMin() {
    let window = BrowserWindow.getFocusedWindow();
    window.minimize();
  }

  onMax() {
    let window = BrowserWindow.getFocusedWindow();
    if (window.isMaximized()) {
      window.unmaximize();
    } else {
      window.maximize();
    }
  }
  onClose() {
    let window = BrowserWindow.getFocusedWindow();
    window.close();
  }
}


